---
title: "Connecter un Grafana local avec un Prometheus distant sur Kubernetes"
date: 2021-10-18T20:57:15+02:00
draft: false
tags: ['grafana', 'prometheus', 'kubernetes']
---

## Configuration du port forwarding

Dans k9s afficher la vue Pod et créer un port forwarding (`shift` + `f`) sur le pod `prometheus-deployment-xxxxxxxxxx-yyyyz` avec les paramètres suivants :

- mapping de port : `9090:9090`
- url : `http://192.168.1.2:9090/`

Remplacer `192.168.1.2` par l'adresse IP privée du poste local.

## Installation et configuration de grafana

### Installation

Déployer grafana en local avec docker :

```nohighlight
docker run -d --name grafana grafana/grafana
```

### Connexion

Se connecter au grafana avec la commande suivante :

```nohighlight
firefox http://172.17.0.2:3000
```

- utilisateur : `admin`
- mot de passe : `admin`

Remplacer `172.17.0.2` par l'adresse IP interne du conteneur grafana. On peut récupérer cette adresse avec la commande :

```nohighlight
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' grafana
```


### Datasource

Créer une datasource de type Prometheus avec les paramètres suivants :

- url : `http://192.168.1.2:9090`
- Access : `Server`

Remplacer `192.168.1.2` par l'adresse IP privée du poste local.

### Dashboards

Importer les dashboards grafana suivants :

- `9679` : Kubernetes Cluster
- `9797` : Kubernetes Deployments
- `9614` : Nginx Controller

Et voilà !

