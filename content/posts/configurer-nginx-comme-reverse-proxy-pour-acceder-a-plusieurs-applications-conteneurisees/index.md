---
title: "Configurer Nginx comme reverse proxy pour accéder à plusieurs applications conteneurisées"
date: 2022-01-05T21:06:42+02:00
draft: false
tags: ['docker', 'nginx', 'reverse proxy']
---

Dans cet exemple nous créons un réseau bridge sur lequel nous connectons 3 conteneurs :

- wit-blue
- wit-pink
- nginx

Seul le conteneur nginx sera exposé sur le port 80 et nginx va appliquer les règles suivantes :

- le trafic arrivant sur le path `/blue` sera routé vers le service `wit-blue`
- le trafic arrivant sur le path `/pink` sera routé vers le service `wit-pink`

Nous utiliserons ce fichier de configuration Nginx :

{{% highlight-bundle-file "nginx.conf" "nginx" %}}


Voici le script permettant de dérouler l'exemple :

{{% highlight-bundle-file "demo-up" "bash" %}}

Pour lancer la démo jouer la commande suivante :

```none
bash demo-up
```

Et le script de nettoyage :
{{% highlight-bundle-file "demo-down" "bash" %}}

Pour lancer le nettoyage :

```none
bash demo-down
```

