---
title: "Programmer l'extinction automatique d'applications sur un cluster Kubernetes avec Kube-green"
date: 2024-08-05T21:22:05+02:00
draft: false
tags: ['kubernetes', 'greenops', 'finops', 'kube-green', 'cert-manager']
---

[Kube-green](https://kube-green.dev/) est un opérateur _GreenOps_ pour réduire l'empreinte CO2 d'un cluster Kubernetes.

>Le GreenOps est la pratique consistant à minimiser l’empreinte carbone d’un environnement cloud grâce à l’utilisation efficace des ressources deployées sur cet environnement.  
--  
<small>[Forrester - GreenOps, FinOps, And The Sustainable Cloud](https://www.forrester.com/blogs/greenops-finops-and-the-sustainable-cloud/)</small>

Kube-green permet de programmer l'extinction automatique d'applications sur certaines plages horaires. Cela peut être utile pour décharger le cluster de certaines applications qui ne sont utilisées qu'en journée par exemple, ou certains jours de la semaine uniquement. Couplé au fonctionnalités [Descheduler](https://github.com/kubernetes-sigs/descheduler) et [Cluster Autoscaling](https://kubernetes.io/docs/concepts/cluster-administration/cluster-autoscaling/) il devient possible d'ajuster le nombre de noeuds sur le cluster à la baisse ce qui en plus d'éviter de gâcher inutilement des ressources peut faire économiser jusqu'à 40% de la facture sur un cluster managé sur GCP ou AWS.

Kube-green permet de [définir des heures de fonctionnement](https://kube-green.dev/docs/configuration/) pour les pods d'un namespace donné. Ceci se déclare très simplement à l'aide d'une ressource de type `SleepInfo` comme dans l'exemple ci-dessous :

```yaml
apiVersion: kube-green.com/v1alpha1
kind: SleepInfo
metadata:
  name: working-hours
  namespace: dev
spec:
  weekdays: "1-5"
  sleepAt: "19:00"
  wakeUpAt: "09:00"
  timeZone: "Europe/Paris"
```

Ici on indique que les pods du namespace `dev` seront actifs `du lundi au vendredi entre 9h et 19h`. En dehors de cette plage les pods seront automatiquement "éteints" par Kube-green.

Pour "éteindre" les pods, Kube-green passe simplement le nombre de réplicas des déploiements à zéro. Et pour les rallumer, il rétablit la valeur initiale.

A ce jour Kube-green est capable de gérer les pods managés par :

- un Deployment
- un CronJob

Si besoin Kube-green permet de définir une liste d'exclusion pour les applications qui doivent rester actives en dehors de la plage de fonctionnement.

## Installation de Kube-green

L'installation de Kube-green n'est pas aussi rapide que l'exemple fourni sur [la page d'installation](https://kube-green.dev/docs/install/) officielle. Kube-green nécessite en effet un certificat TLS pour pouvoir démarrer, et cette partie n'est pas détaillée de façon très explicite dans la documentation du produit.

Il faut donc commencer par déployer [cert-manager](https://cert-manager.io/) sur le cluster :

```nohightlight
kubectl apply -f https://github.com/jetstack/cert-manager/releases/latest/download/cert-manager.yaml
```

Une fois que cert-manager est déployé, nous allons donc appliquer sur le cluster les ressources nécessaires pour créer :

- un `Issuer` pour Kube-green,
- un `Certificate` signé par l'issuer pour le service Green-kube.

Ces ressources sont à déployer dans le namespace `kube-green` qui est le namespace dans lequel Kube-green sera déployé :

```nohightlight
kubectl create namespace kube-green
```

`certs/kube-green-selfsigned-issuer.issuer.yaml`

```yaml
apiVersion: cert-manager.io/v1
kind: Issuer
metadata:
  name: kube-green-selfsigned-issuer # Do no change this, the name is targeted by kube-green
  namespace: kube-green
spec:
  selfSigned: {}
```

`certs/kube-green-serving-cert.certificate.yaml`

```yaml
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: kube-green-serving-cert # Do no change this, the name is targeted by kube-green
  namespace: kube-green
spec:
  usages:
    - server auth
    - client auth
  dnsNames:
    - "kube-green-webhook-service"
    - "kube-green-webhook-service.kube-green"
    - "kube-green-webhook-service.kube-green.svc"
    - "kube-green-webhook-service.kube-green.svc.cluster.local"
  secretName: webhook-server-cert
  issuerRef:
    name: kube-green-selfsigned-issuer
```

On applique l'ensemble des fichiers sur le cluster :

```nohightlight
kubectl apply -f certs/
```

Puis on déploie Kube-Green :

```nohightlight
kubectl apply -f https://github.com/kube-green/kube-green/releases/latest/download/kube-green.yaml
```

Pour vérifier que l'installation est réalisée avec succès on peut consulter les logs de Kube-green

```nohightlight
kubectl -n kube-green logs deployments/kube-green-controller-manager
```

## Définition de la plage autorisée pour les applications

Il ne reste plus qu'à provisionner la ressource `SleepInfo` déjà présentée en introduction de l'article :

`working-hours.sleepinfo.yaml`

```yaml
apiVersion: kube-green.com/v1alpha1
kind: SleepInfo
metadata:
  name: working-hours
  namespace: dev
spec:
  weekdays: "1-5"
  sleepAt: "19:00"
  wakeUpAt: "09:00"
  timeZone: "Europe/Paris"
```

```nohightlight
kubectl apply -f working-hours.sleepinfo.yaml
```

Avec cette configuration les Deployments du namespace `dev` verront leur nombre de réplicas "magiquement" réduits à zéro les jours de semaine entre 19h et 9h le lendemain, mais aussi durant le week-end.

Youpi !
