---
title: "Scripter une démo en ligne de commande avec Demo Magic"
date: 2023-07-03T08:01:17+02:00
draft: false
tags: ['bash', "demo", 'demo-magic']
---

## Introduction

[Demo Magic](https://github.com/paxtonhare/demo-magic) permet de scripter à l'avance une démo en ligne de commande afin de pouvoir la rejouer simplement à tout moment en évitant les erreurs et les traditionnelles typos inhérentes à ce genre d'exercice.

## Pré-requis

Téléchargement du script `demo-magic.sh` et installation de sa dépendance `pv` :

```bash
mkdir demo-magic && cd demo-magic
curl -LO https://raw.githubusercontent.com/paxtonhare/demo-magic/master/demo-magic.sh
sudo apt update && sudo install -y pv
```

Voilà c'est tout !

## Utilisation

C'est très simple, voici une petite démo d'exemple :

`ma-demo.sh`

```bash
#!/bin/bash

# --- Chargement de Demo Magic

. demo-magic.sh
clear


# --- Configuration -----------------------------------------------------------

DEMO_PROMPT="${BLUE}\W${WHITE} ➜ ${COLOR_RESET}"


# --- Ma démo -----------------------------------------------------------------

pei "# Bienvenue sur ma démo 😀"

pei "# Démarrage du conteneur docker :"
pei "docker run -d --name wit xian310/who-is-there:25"
pei "docker ps"
wait

pei "# Récupération de l'adresse IP :"
pei 'CONTAINER_IP=$(docker container inspect wit -f "{{ json .NetworkSettings.IPAddress }}")'
pei 'echo "$CONTAINER_IP"'
wait

pei "# Test de connexion :"
pei "curl $CONTAINER_IP:8080"
wait

pei "# Destruction du conteneur :"
pei "docker rm -f wit"
pei "docker ps"

p "# c'est fini 🤩"
```

Nous pouvons ensuite lancer directement la démo avec la commande :

```bash
bash ma-demo.sh
demo-magic ➜ # Bienvenue sur ma démo 😀
demo-magic ➜ # Démarrage du conteneur docker :
demo-magic ➜ docker run -d --name wit xian310/who-is-there:25
1391cfaccf7761b80a23e1c724d1fbe67f80928a33d790f01a2b720d5668f429
demo-magic ➜ docker ps
CONTAINER ID   IMAGE                     COMMAND           CREATED         STATUS                  PORTS      NAMES
1391cfaccf77   xian310/who-is-there:25   "/who-is-there"   2 seconds ago   Up Less than a second   8080/tcp   wit
demo-magic ➜ # Récupération de l'adresse IP :
demo-magic ➜ CONTAINER_IP=$(docker container inspect wit -f "{{ json .NetworkSettings.IPAddress }}")
demo-magic ➜ echo "$CONTAINER_IP"
"172.17.0.2"
demo-magic ➜ # Test de connexion :
demo-magic ➜ curl "172.17.0.2":8080
wit v25 - Hello from 1391cfaccf77
demo-magic ➜ # Destruction du conteneur :
demo-magic ➜ docker rm -f wit
wit
demo-magic ➜ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
demo-magic ➜ # c'est fini 🤩
```

Magique non ?
