---
title: "Agrégation de logs de conteneurs Docker avec Loki / loki-docker-driver et visualisation dans Grafana"
date: 2021-10-15T18:37:29+02:00
draft: false
tags: ['docker', 'loki', 'loki-docker-driver', 'grafana']
---

Sources :

- https://yuriktech.com/2020/03/21/Collecting-Docker-Logs-With-Loki/
- https://blog.ruanbekker.com/blog/2020/08/13/getting-started-on-logging-with-loki-using-docker/
- https://thesmarthomejourney.com/2021/08/23/loki-grafana-log-aggregation/
- https://itnext.io/monitoring-your-docker-containers-logs-the-loki-way-e9fdbae6bafd

## loki-docker-driver

[Loki Docker Driver Client](https://grafana.com/docs/loki/latest/clients/docker-driver/) est un plugin de log pour Docker qui permet la collecte des logs émis par les conteneurs Docker.

Installation :

```nohighlight
$ docker plugin install grafana/loki-docker-driver:latest \
    --alias loki \
    --grant-all-permissions
latest: Pulling from grafana/loki-docker-driver
Digest: sha256:1dc6bf5eddc296b6a02142d01ae3974fa74911ef76bcdd3ac22681ae5c6c73a1
901eaf978be4: Complete 
Installed plugin grafana/loki-docker-driver:latest
```
```nohighlight
$ sudo systemctl restart docker
```
```nohighlight
$ docker plugin ls
ID             NAME          DESCRIPTION           ENABLED
9d45cd54a362   loki:latest   Loki Logging Driver   true
```

## loki / grafana / applications de démonstration

L'utilisation du driver Loki peut se configurer globalement ou directement pour chaque service dans le fichier `docker-compose.yaml` de la façon suivante : 

```yaml
  my-service:
    image: my-image
    logging:
      driver: loki
      options:
        loki-url: http://localhost:3100/loki/api/v1/push
        loki-external-labels: job=dockerlogs,my-label=my-value,owner=,environment=dev
```

Les labels ainsi définis pourront être exploités pour filtrer les logs.

Déployer la stack de démonstration :

{{% highlight-bundle-file "docker-compose.yaml" "yaml" %}}

```nohighlight
$ docker-compose up -d
```

Se connecter sur grafana :

```nohighlight
$ firefox http://127.0.0.1:3000/
```

Créer une datasource de type Loki :

- Name : Loki
- URL  : http://loki:3100

Importer le Dashboard [grafana-dashboard.json](grafana-dashboard.json).

## Générer des logs

La commande suivante permet de générer des logs sur les 2 services de démonstration déployés par la stack docker-compose :

```nohighlight
for i in {1..10000} ; do curl "http://127.0.0.1:808$(( $RANDOM % 2 ))/$(apg -q -n 1 -M l)" && sleep 1 ; done
```

## logCLI

Outil en ligne de commande pour interroger Loki.

Téléchargement : https://github.com/grafana/loki/releases

Configuration

```nohighlight
$ #export LOKI_USERNAME=${MYUSER}
$ #export LOKI_PASSWORD=${MYPASS}
$ export LOKI_ADDR=http://localhost:3100
```

Utilisation

```nohighlight
$ logcli query '{job="dockerlogs"}'
```
