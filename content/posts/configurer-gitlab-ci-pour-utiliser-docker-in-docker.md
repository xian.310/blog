---
title: "Configurer Gitlab-CI pour utiliser Docker in Docker"
date: 2022-02-04T21:22:05+02:00
draft: false
tags: ['gitlab-ci', 'docker', 'docker in docker']
---

## Configuration du Runner

On utilise `privileged = true` plutôt que le montage de la socket `/var/run/docker.sock` pour les raisons expliquées dans le post suivant :

[https://applatix.com/case-docker-docker-kubernetes-part-2/](https://applatix.com/case-docker-docker-kubernetes-part-2/)


Contenu du fichier `/etc/gitlab-runner/config.toml` :

```toml
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "Mon runner"
  url = "https://gitlab.example.com"
  token = "**********************"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "docker:20"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0
```

## Utilisation dans un job Gitlab-CI

Avec certaines version de l'image `docker:dind` on peut obtenir le message d'erreur suivant :

```
Cannot connect to the Docker daemon at tcp://docker:2375. Is the docker daemon running?
```

La raison de cette erreur est bien expliqué dans ce post :

[https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27300#note_466755332](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27300#note_466755332)

Il faut donc configurer le job de la manière suivante :

```yaml
dind-test:
  stage: dind
  image: docker:20.10
  services:
    - name: docker:20.10-dind
      # explicitly disable tls to avoid docker startup interruption
      command: [ "--tls=false" ]
  variables:
    # Docker daemon is exposed by a service with no TLS.
    DOCKER_HOST: "tcp://docker:2375"
    # Instruct Docker not to start over TLS.
    DOCKER_TLS_CERTDIR: ""
    # Improve performance with overlay fs.
    DOCKER_DRIVER: overlay2
  before_script:
    # logging in into Docker Registry on GitLab using GitLab CI/CD variables
    - echo -n $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
  after_script:
    - docker logout $CI_REGISTRY
  script:
    - docker info
```
