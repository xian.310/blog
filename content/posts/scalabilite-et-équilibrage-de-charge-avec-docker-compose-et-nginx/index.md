---
title: "Scalabilité et équilibrage de charge avec Docker Compose et Nginx"
date: 2022-01-12T21:14:36+02:00
draft: false
tags: ['scalabilité', 'load balancing', 'docker', 'docker compose', 'nginx', 'reverse proxy']

---

Les fichiers :

{{% highlight-bundle-file "docker-compose.yml" "yaml" %}}

{{% highlight-bundle-file "nginx.conf" "nginx" %}}

Mise en place :

```
$ docker-compose up --scale wit-blue=3  -d
Creating network "wit-net" with driver "bridge"
Creating docker-compose-load-balancing_wit-blue_1 ... done
Creating docker-compose-load-balancing_wit-blue_2 ... done
Creating docker-compose-load-balancing_wit-blue_3 ... done
Creating docker-compose-load-balancing_nginx_1    ... done
```

Test :

```
$ for i in {1..10} ; do curl http://127.0.0.1:8000 ; done
wit v21 - Hello from aba3fa1cee89
wit v21 - Hello from 2c6dc415a207
wit v21 - Hello from f69bcc96af12
wit v21 - Hello from aba3fa1cee89
wit v21 - Hello from 2c6dc415a207
wit v21 - Hello from f69bcc96af12
wit v21 - Hello from aba3fa1cee89
```
