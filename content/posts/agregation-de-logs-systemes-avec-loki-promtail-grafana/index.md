---
title: "Agrégation de logs systèmes avec Loki, Promtail  et Grafana"
date: 2021-10-11T18:37:29+02:00
draft: false
tags: ['loki', 'promtail', 'grafana']
---

Dans cet exemple les composants Loki, Promtail  et Grafana sont déployés à l'aide de Docker Compose. Voici la définition du fichier :

{{% highlight-bundle-file "docker-compose.yaml" "yaml" %}}

Promtail est configuré de manière à agréger les logs présents dans `/var/log/*log` et dans le fichier `/tmp/log/date.log`. Les logs des deux sources seront présentés comme provenant de deux machines différentes :

{{% highlight-bundle-file "promtail_config.yml" "yaml" %}}

