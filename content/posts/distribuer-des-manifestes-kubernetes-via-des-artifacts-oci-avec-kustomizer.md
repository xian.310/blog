---
title: "Distribuer des manifestes kubernetes via des artifacts OCI avec Kustomizer"
date: 2023-07-18T13:13:17+02:00
draft: false
tags: ['infrastructure as code', 'kubernetes', "kustomize", 'kustomizer', 'artifact', 'oci', 'registry']
---

## Introduction

[Kustomizer](https://github.com/stefanprodan/kustomizer) est un outil super pratique permettant de packager, de distribuer et déployer des fichiers manifestes Kubernetes via des artifacts OCI. Concrètement, cela permet de publier et de versionner les fichiers de déploiement d'une application Kubernetes sur un registry Docker, de la même manière que ce qu'on fait habituellement pour les images Docker des applications. On peut ainsi avoir côte à côte dans le même registry l'image applicative et sa configuration pour un environnement de déploiement donné.

Kustomizer est un outil écrit par l'excellent [Stefan Prodan](https://github.com/stefanprodan) qui n'est autre que le maître d'oeuvre derrière les solutions gitops Flux et Flagger.

## Fichiers manifestes de l'application

Pour la suite de cet article nous utilisons les manifestes Kubernetes d'une application packagée au format [Kustomize](https://kustomize.io/) avec la structure suivante :

```yaml
wit-scratch/
├── base
│   ├── config.yaml
│   ├── kustomization.yaml
│   ├── wit.deployment.yaml
│   └── wit.service.yaml
└── overlays
    ├── prod
    │   └── kustomization.yaml
    └── staging
        └── kustomization.yaml
```

La définition déclarée dans le dossier `base` peut être "patchée" pour un déploiement sur deux environnements distincts : définis dans les deux overlays `staging` et `prod`.

A titre d'expemple le fichier `overlays/staging/kustomization.yaml` ressemble à ceci :

```yaml
---
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
resources:
- ../../base

namespace: default

patches:
- |-
  apiVersion: apps/v1
  kind: Deployment
  metadata:
    name: wit
  spec:
    replicas: 2
    template:
      spec:
        containers:
        - name: wit
          env:
          - name: COLOR
            value: DarkOrange


images:
- name: who-is-there
  newName: c8n.io/xian/who-is-there
  newTag: "24"
```

Ce fichier permet de patcher pour l'environnement de staging le nombre de replicas, la valeur de la variable COLOR ainsi que le nom et le tag de l'image Docker à utiliser pour le déploiement de l'application.

## Utilisation de Kustomizer

Une fois Kustomizer installé, l'outil va nous permettre de réaliser très simplement les actions suivantes :

- Créer un artifact par environnement, chaque artifact contenant les fichiers manifestes de l'application à déployer pour cet environnement.
- Pousser ces artifacts sur un registry Docker. Dans les exemples suivants nous utiliserons le registry [c8n.io](https://c8n.io).
- Déployer l'application sur les différents environnements.
- Mettre à jour l'application.
- Voir le différentiel entre ce qui est déployé et la nouvelle version.
- Supprimer les applications.

### Créer et pousser les artifacts

Les commandes ci-dessous vont nous permettre de produire des artifacts OCI contenant les fichiers de déploiement pour chacun des environnements cibles. Ces artificats seront automatiquement publiés sur le registry Docker `c8n.io`.

Pour l'environnement staging :

```none
$ kustomizer push artifact oci://c8n.io/xian/who-is-there-staging:24.0.0 \
   -k wit-scratch/overlays/staging/
building manifests...
ConfigMap/default/wit-scratch-87dfh6926g
Service/default/wit-scratch
Deployment/default/wit-scratch
pushing image c8n.io/xian/who-is-there-staging:24.0.0
published digest c8n.io/xian/who-is-there-staging@sha256:d81cb83f6cffe58c8bce9af21aed4d87f6fa4078a6885aacae340fac3d6bff2c
```

Pour l'environnement prod :

```none
$ kustomizer push artifact oci://c8n.io/xian/who-is-there-prod:24.0.0 \
    -k wit-scratch/overlays/prod/
building manifests...
ConfigMap/default/wit-scratch-87dfh6926g
Service/default/wit-scratch
Deployment/default/wit-scratch
pushing image c8n.io/xian/who-is-there-prod:24.0.0
published digest c8n.io/xian/who-is-there-prod@sha256:c20c84f52e93be4e1bedb96d2967c02d8b583daa0b8a2aacb8ae47800c4c2d74
```

Vérifions que les artifacts sont disponibles sur le registry :

```none
$ kustomizer list artifact oci://c8n.io/xian/who-is-there-staging
VERSION	URL                                     
24.0.0 	c8n.io/xian/who-is-there-staging:24.0.0
```

```none
$ kustomizer list artifact oci://c8n.io/xian/who-is-there-prod
VERSION	URL                                  
24.0.0 	c8n.io/xian/who-is-there-prod:24.0.0
```

### Déployer l'application

Toujours avec Kustomizer nous pouvons désormais déployer l'application. Par exemple sur l'environnement "Staging", la commande suivante créé un `inventory` nommé `wit-staging` associé à un numéro de révision :

```none
$ kustomizer apply inventory wit-staging \
    --prune \
    --artifact oci://c8n.io/xian/who-is-there-staging:24.0.0 \
    --revision 24.0.0
building inventory...
applying 3 manifest(s)...
ConfigMap/default/wit-scratch-87dfh6926g created
Service/default/wit-scratch created
Deployment/default/wit-scratch created
```

A noter que Kustomizer génère automatiquement une ressource ConfigMap lui permettant de conserver la trace de l'ensemble des références des ressources déployées afin de pouvoir faire des nettoyages propres lors des mises à jour ou suppression...

On peut à tout moment "inspecter" l'inventory pour examiner quels sont les ressources correspondantes sur le cluster :

```none
$ kustomizer inspect inventory wit-staging
Inventory: default/wit-staging
LastAppliedAt: 2023-07-18T11:45:02Z
Revision: 24.0.0
Artifacts:
- oci://c8n.io/xian/who-is-there-staging@sha256:d81cb83f6cffe58c8bce9af21aed4d87f6fa4078a6885aacae340fac3d6bff2c
Resources:
- ConfigMap/default/wit-scratch-87dfh6926g
- Service/default/wit-scratch
- Deployment/default/wit-scratch
```

### Mettre à jour l'application

Effectuons un changement sur le manifeste du déploiement, par exemple modifions la valeur de la variable COLOR, et repoussons une nouvelle version d'artifact sur le registry :

```none
$ kustomizer push artifact oci://c8n.io/xian/who-is-there-staging:24.0.1 -k wit-scratch/overlays/staging/
building manifests...
ConfigMap/default/wit-scratch-87dfh6926g
Service/default/wit-scratch
Deployment/default/wit-scratch
pushing image c8n.io/xian/who-is-there-staging:24.0.1
published digest c8n.io/xian/who-is-there-staging@sha256:f260823d0f498f517774312e0b1e685828457514434eaa3f75dcdd41eb6b1466
```

Nous avons maintenant deux versions distinctes des manifestes sur le registry :

```none
$ kustomizer list artifact oci://c8n.io/xian/who-is-there-staging
VERSION	URL                                     
24.0.1 	c8n.io/xian/who-is-there-staging:24.0.1	
24.0.0 	c8n.io/xian/who-is-there-staging:24.0.0	
```

La commande `kustomizer diff` permet d'analyser les différences entre la version actuellement déployée sur le cluster et la nouvelle version que nous venons de pousser sur le registry :

```diff
$ kustomizer diff inventory wit-staging \
    --prune \
    --artifact oci://c8n.io/xian/who-is-there-staging:24.0.1
► Deployment/default/wit-scratch drifted
@@ -4,7 +4,7 @@
   annotations:
     deployment.kubernetes.io/revision: "1"
   creationTimestamp: "2023-07-18T11:45:02Z"
-  generation: 1
+  generation: 2
   labels:
     inventory.kustomizer.dev/name: wit-staging
     inventory.kustomizer.dev/namespace: default
@@ -33,7 +33,7 @@
       containers:
       - env:
         - name: COLOR
-          value: Orange
+          value: DarkOrange
         image: c8n.io/xian/who-is-there:24
         imagePullPolicy: IfNotPresent
         name: wit
```

Appliquons la nouvelle version :

```none
$ kustomizer apply inventory wit-staging \
    --prune \
    --artifact oci://c8n.io/xian/who-is-there-staging:24.0.1 \
    --revision 24.0.1
building inventory...
applying 3 manifest(s)...
ConfigMap/default/wit-scratch-87dfh6926g unchanged
Service/default/wit-scratch unchanged
Deployment/default/wit-scratch configured
```

### Supprimer l'application

Enfin on pourra supprimer l'ensemble des fichiers du cluster de façon aussi simple :

```none
$ kustomizer delete inventory wit-staging
retrieving inventory...
deleting 3 manifest(s)...
Deployment/default/wit-scratch deleted
Service/default/wit-scratch deleted
ConfigMap/default/wit-scratch-87dfh6926g deleted
ConfigMap/default/wit-staging deleted
waiting for resources to be terminated...
all resources have been deleted
```
