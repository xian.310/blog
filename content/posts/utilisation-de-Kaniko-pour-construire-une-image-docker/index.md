---
title: "Utilisation de Kaniko pour construire une image Docker"
date: 2021-11-21T21:01:03+02:00
draft: false
tags: ['kaniko', 'docker']
---

Dans cet exemple, nous construisons une image Docker avec Kaniko et nous la poussons sur un registry local temporaire.


Notre Dockerfile minimaliste :

{{% highlight-bundle-file "Dockerfile" "docker" %}}

Le script d'automatisation de l'exemple effectue les actions suivantes :

1. création d'un registry Dcker local temporaire
2. construction de l'image Docker à l'aide de Kaniko
3. envoie de l'image sur le registry local
4. récupération de l'image depuis le registry local

{{% highlight-bundle-file "build-image-with-kaniko" "bash" %}}

Nous pouvons lancer l'exemple à l'aide de la commande :

```nohighlight
$ bash build-image-with-kaniko
```
