---
title: "Métriques personnalisées avec GKE"
date: 2022-02-17T21:24:14+02:00
draft: false
tags: ['metriques', 'gke', 'gcp', podmonitor', 'kube-state-metrics']
---

## Récupérer des métriques personnalisées

Documentation officielle :
* https://cloud.google.com/stackdriver/docs/solutions/gke/managing-metrics#configure-workload-metrics

L'option de cluster GKE "workload-metrics" déploie :

- un daemonset "workload-metrics"
- une CRD "pod-monitor"
- on peut ensuite scrapper les métriques au format prométheus d'un service donné en créant une ressource de type PodMonitor

### Exemple pour flux

PodMonitor pour récupérer les métriques de flux :

```nohighlight
$ kubectl get podmonitors -n flux-system flux-system -o yaml`
```

```yaml
apiVersion: monitoring.gke.io/v1alpha1
kind: PodMonitor
metadata:
  labels:
    app.kubernetes.io/part-of: flux
    kustomize.toolkit.fluxcd.io/name: flux-system
    kustomize.toolkit.fluxcd.io/namespace: flux-system
  name: flux-system
  namespace: flux-system
spec:
  podMetricsEndpoints:
  - port: http-prom
  selector:
    matchExpressions:
    - key: app
      operator: In
      values:
      - helm-controller
      - source-controller
      - kustomize-controller
      - notification-controller
      - image-automation-controller
      - image-reflector-controller
```

### Exemple pour kube-state-metric

PodMonitor pour récupérer les métriques de kube-state-metric :

```nohighlight
$ kubectl get podmonitors -n kube-system kube-state-metrics -o yaml
```

```yaml
apiVersion: monitoring.gke.io/v1alpha1
kind: PodMonitor
metadata:
  labels:
    kustomize.toolkit.fluxcd.io/name: flux-system
    kustomize.toolkit.fluxcd.io/namespace: flux-system
  name: kube-state-metrics
  namespace: kube-system
spec:
  podMetricsEndpoints:
  - path: /metrics
    port: http-metrics
    scheme: http
  selector:
    matchLabels:
      app.kubernetes.io/name: kube-state-metrics
```

Attention les métriques sont facturées. Il est possible de filtrer certaines métriques avec le paramètre `metricRelabelings` dans le PodMonitor. plus de détails :

* https://cloud.google.com/stackdriver/docs/solutions/gke/managing-metrics#managing-costs

