#!/bin/bash

# Get the Open ID token
token=$(
curl -s -k -X POST https://192.168.1.47:8443/auth/realms/master/protocol/openid-connect/token \
  -d grant_type=password \
  -d client_id=kubernetes-cluster \
  -d username=xian \
  -d password=pass \
  -d scope=openid \
  -d response_type=id_token \
  | jq -r .id_token
)

kubectl --token $token "$@"
  
