#!/bin/bash                      

# Set credentials for kubernetes user 

idp_issuer_url=https://192.168.1.47:8443/auth/realms/master
idp_ca=/home/xian/workspace/kubernetes-openid-connect/rootCA.crt
client_id=kubernetes-cluster                                                                       
client_secret=r60pzHf9dQt4TgcnM0dA6FUUEwqoTKzj
user_name=xian
user_password=pass
debug=True

echo "Get the Open ID token..."
token=$(
curl -s -k -X POST $idp_issuer_url/protocol/openid-connect/token \
  -d grant_type=password \
  -d client_id=$client_id \
  -d username=$user_name \
  -d password=$user_password \
  -d scope=openid \
  -d response_type=id_token \
  -d client_secret=$client_secret
)

[ $debug == True ] && echo $token | jq .

id_token=$(echo $token | jq -r .id_token)
refresh_token=$(echo $token | jq -r .refresh_token)

kubectl config set-credentials $user_name \
   --auth-provider=oidc \
   --auth-provider-arg=idp-issuer-url=$idp_issuer_url \
   --auth-provider-arg=client-id=$client_id \
   --auth-provider-arg=client-secret=$client_secret \
   --auth-provider-arg=refresh-token=$refresh_token \
   --auth-provider-arg=idp-certificate-authority=$idp_ca \
   --auth-provider-arg=id-token=$id_token
