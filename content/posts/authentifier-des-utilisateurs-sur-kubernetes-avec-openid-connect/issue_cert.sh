#!/bin/bash 

# https://gist.github.com/fntlnz/cf14feb5a46b2eda428e000157447309
# https://www.scaleway.com/en/docs/network/load-balancer/how-to/create-self-signed-tls-certificate/

set -e

openssl req -new -newkey rsa:4096 -nodes -keyout cert.key -out cert.csr -subj "/C=FR/ST=Bretagne/L=Rennes/O=XianLabs/CN=www.example.com"

openssl x509 -req -extfile alt_names.txt -sha256 -days 365 -in cert.csr -CA rootCA.crt -CAkey rootCA.key -CAcreateserial -out cert.crt

