#!/bin/bash

set -e

openssl genrsa -out rootCA.key 4096
openssl req -x509 -sha256 -new -nodes -key rootCA.key -days 3650 -out rootCA.crt

