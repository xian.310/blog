#!/bin/bash

# Get the Open ID token
curl -s -k -X POST https://192.168.1.47:8443/auth/realms/master/protocol/openid-connect/token \
  -d grant_type=password \
  -d client_id=kubernetes-cluster \
  -d username=xian \
  -d password=pass \
  -d scope=openid \
  -d response_type=id_token \
  | jq -r .id_token > token

# Try to connect on Kubernetes API server
curl -H "Authorization: Bearer $(< token)" -k https://127.0.0.1:35081/api/v1/namespaces

