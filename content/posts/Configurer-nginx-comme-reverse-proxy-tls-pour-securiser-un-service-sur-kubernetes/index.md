---
title: "Configurer Nginx comme reverse proxy TLS pour sécuriser un service sur Kubernetes"
date: 2023-03-02T07:06:42+02:00
draft: false
tags: ['kubernetes', 'nginx', 'reverse proxy', 'tls']
---

Lorsqu'on souhaite déployer un service sur Kubernetes et le protéger via TLS, la méthode la plus simple est généralement de s'appuyer sur la solution d'ingress controller existante sur le cluster.

Mais dans certains cas on doit intervenir sur des clusters qui sont gérés par d'autres équipes et sur lequels aucune solution d'ingress n'est mise en place pour gérer le traffic entrant. De plus sur ce type de cluster on n'a pas forcément la lattitude de pouvoir faire ce qui nous plaît faute de droits suffisants.

Dans ce cas, une solution est de déployer soi-même un pod Nginx et de le configurer pour qu'il porte la terminaison TLS de notre service.


## Gestion des certificats

Première étape, créons notre autorité de certification :

```none
openssl genrsa -out rootCA.key 4096
openssl req -x509 -sha256 -new -nodes -key rootCA.key -days 3650 -out rootCA.crt
```

Puis le certificat pour notre service :

```none
openssl req -new -newkey rsa:4096 -nodes -keyout cert.key -out cert.csr -subj "/C=FR/O=Xian-Labs/CN=mon-service.example.com"
openssl x509 -req -sha256 -days 365 -in cert.csr -CA rootCA.crt -CAkey rootCA.key -CAcreateserial -out cert.crt
```

Vérifions le certificat produit :

```none
openssl x509 -in cert.crt -text -noout
```

## Configuration Nginx

Nous utilisons le fichier de configuration suivant pour Nginx :

{{% highlight-bundle-file "default.conf" "nginx" %}}

Ce fichier nous permet de définir l'emplacement des certificats TLS ainsi que le service vers lequel nous souhaitons proxifier le traffic via la directive :

```none
proxy_pass http://mon-service
```

Nous allons ensuite créer les élements de configuration pour notre Pod Nginx :

1. un Secret qui contiendra les certificats TLS

    ```none
    kubectl create secret tls mon-service-tls --cert cert.crt --key cert.key
    ```

2. un ConfigMap qui contiendra le fichier de configuration Nginx décrit ci-dessus

    ```none
    kubectl create configmap mon-service-tls --from-file default.conf
    ```

## Déploiement du reverse proxy Nginx

Nous déclarons un Deployment ainsi qu'un Service Kubernetes de type NodePort :

{{% highlight-bundle-file "nginx-tls.deployment.yaml" "yaml" %}}

{{% highlight-bundle-file "nginx-tls.service.yaml" "yaml" %}}

```none
kubectl apply -f nginx-tls.deployment.yaml
kubectl apply -f nginx-tls.service.yaml
```

Et voilà, notre service est protégé avec TLS ;-)