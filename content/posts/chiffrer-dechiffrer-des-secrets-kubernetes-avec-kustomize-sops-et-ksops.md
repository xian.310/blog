---
title: "Chiffrer et déchiffrer des secrets kubernetes avec Kustomize, SOPS et KSOPS"
date: 2022-03-11T21:28:41+02:00
draft: false
tags: ['gestion des secrets', 'kubernetes', 'kustomize', 'sops', 'ksops']
---

KSOPS est un plugin Kustomize qui permet de déchiffrer des secrets Kubernetes chiffrés avec SOPS

Ces outils ont l'énorme avantage de permettre le stockage de secrets Kubernetes (chiffrés) directement dans le dépôt de code.

## Prérequis

SOPS et Kustomize doivent être installés :

- SOPS : https://github.com/mozilla/sops
- Kustomize : https://github.com/kubernetes-sigs/kustomize

## Installation de KSOPS

Site officiel de KSOPS : https://github.com/viaduct-ai/kustomize-sops


Si elle n'existe pas déjà, définir la variable `XDG_CONFIG_HOME` :

```nohighlight
echo "export XDG_CONFIG_HOME=\$HOME/.config" >> $HOME/.bashrc
```

Installer KSOPS

```nohighlight
curl -s https://raw.githubusercontent.com/viaduct-ai/kustomize-sops/master/scripts/install-ksops-archive.sh | bash
```

Le script vient installer le plugin KSOPS pour Kustomize dans le dossier `~/.config/kustomize/plugin` :

```nohighlight
$ tree ~/.config/kustomize
/home/me/.config/kustomize/plugin/
└── viaduct.ai
    └── v1
        └── ksops
            ├── ksops
            ├── LICENSE
            └── README.md
```

## Utilisation de SOPS, Kustomize et KSOPS

### Déclaration de la clé de chiffrement à utiliser

Pour cet exemple nous allons créer [une clé gpg](https://www.redhat.com/sysadmin/creating-gpg-keypairs) pour chiffrer/déchiffrer, mais d'autres types de clés (GPG KMA, AWS KMS, ...) sont également utilisables.

Dans l'overlay Kustomize, créer le fichier `.sops.yaml` :

```yaml
creation_rules:
  - encrypted_regex: "^(data|stringData)$"
    # Specify kms/pgp/etc encryption key
    # This tutorial uses a local PGP key for encryption.
    # DO NOT USE IN PRODUCTION ENV
    pgp: "FBC7B9E2A4F9289AC0C1D4843D16CEE4A27381B4"
    # Optionally you can configure to use a providers key store
    # kms: XXXXXX
    # gcp_kms: XXXXXX
```

L'élément `pgp` permet d'indiquer l'identifiant d'une clé GPG locale. On peut récupérer cet identifiant à l'aide de la commande :

```nohighlight
gpg --list-keys
```

### Chiffrement des secrets avec SOPS

Dans l'overlay Kustomize, créer le secret Kubernetes :

```nohighlight
cat <<EOF > secret.yaml
apiVersion: v1
kind: Secret
metadata:
  name: wit-secret
  annotations:
      kustomize.config.k8s.io/needs-hash: "true"
type: Opaque
data:
  username: d2l0
  password: MTIzNDU2Nzg=
EOF
```

Chiffrer le secret avec SOPS :

```nohighlight
sops -e secret.yaml > secret.enc.yaml
```

Le fichier généré ressemble à ça :

```yaml
cat secret.enc.yaml 
apiVersion: v1
kind: Secret
metadata:
    name: wit-secret
    annotations:
        kustomize.config.k8s.io/needs-hash: "true"
type: Opaque
data:
    username: ENC[AES256_GCM,data:xuJbPw==,iv:ce/ItX4Smcm9VNRAlgoTFEkClaxDe/lnPwP40P/cFXU=,tag:q69fwFE6F1brGPfweJ8Iiw==,type:str]
    password: ENC[AES256_GCM,data:Sodl0wmI8oCblJAt,iv:+VFbeA/GCa4HpPZZ07WqFABhkJU2bMvvsIFmrcAEN5k=,tag:+dyJ8iN781omGK3XY+WWSg==,type:str]
sops:
    kms: []
    gcp_kms: []
    azure_kv: []
    hc_vault: []
    age: []
    lastmodified: "2022-03-30T14:28:35Z"
    mac: ENC[AES256_GCM,data:Z5UgjrFTwL5vxd6RMZ1+U9d3t/t95YwKLomIFnSqBlmS8ciXq1x4sVgFzgtSTAb8xdS31LhKxJT7rK+qJLNGKja8LLjN2QbPbgE+cZd20aKdyUjNmgN67EOcGtW5OKmDMgMoUX2JcoOM2rp1k3ovs7ZPfaNCrs/55ZqzjzVxaDY=,iv:p2jaOdEfnl0mmNki3xibfRDFLRdTeKz9yBoVEczUcpk=,tag:W6GUskSnJgbCfY+ssWATYw==,type:str]
    pgp:
        - created_at: "2022-03-30T14:28:35Z"
          enc: |-
            -----BEGIN PGP MESSAGE-----

            wcBMA+Wdz3Sm3y+DAQgAXaSGZkqCqZLSsys7Y4inzLAUNz8oU3t7HKKIt75WKLFS
            PJgssJli9aYFXDKeATyGO2tATGtSlXmlToZD20fyIXeHj1M7I/qhg+3wYoIiVY//
            s7pmmqsRF4FmjHwuLYzYp+9WMUHfTyYjAubh7VV/pWPKjvSuRwy2H0uxcU8Gqe2u
            eXxgi6FWyWF4RJK9fl3tWqPi6I5d1mf947PbmBTOSSTMd7xAVvItW1iXEU4VQe2c
            ZxOjzXDK8zXYzPbcboNIU/rIiTNetpFA6XuhZr5bLuM9LJvwKlulUVyWDnwYpzCj
            7eoIPmy7eG2HRUeyNzsi0zrxZ9+0ggXoniuqxzaRbdLmAbnLUimQiWz2fq8d9+RQ
            0x9A/1HIArJ3NYpoekUBcldpTBzJ+rfgqnHHJBuer5v+uswjHQnynD8DeVje9SB5
            u+RJyS+XfbwMyvtPU7gO7+kH4utV4HsA
            =SCIW
            -----END PGP MESSAGE-----
          fp: FBC7B9E2A4F9289AC0C1D4843D16CEE4A27381B4
    encrypted_regex: ^(data|stringData)$
    version: 3.7.1
```

Si besoin, le secret peut être déchiffré avec SOPS :

```nohighlight
sops -d secret.enc.yaml 
```

### Déchiffrement des secrets avec Kustomize et KSOPS

Dans l'overlay Kustomize, créer un fichier pour le generator KSOPS :


```nohighlight
cat <<EOF > ksops-secret-generator.yaml
apiVersion: viaduct.ai/v1
kind: ksops
metadata:
  name: ksops-secret-generator
files:
  - secret.enc.yaml
EOF
```

Dans le fichier `kustomization.yaml` ajouter les lignes suivantes pour déclarer le generator créé précédemment :

```yaml
generators:
  - ksops-secret-generator.yaml
```

Générer le manifeste yaml à l'aide de kustomize :

```nohighlight
kustomize build --enable-alpha-plugins .
```

Le secret apparaît bien déchiffré :

```yaml
...
---
apiVersion: v1
data:
  password: MTIzNDU2Nzg=
  username: d2l0
kind: Secret
metadata:
  labels:
    app.kubernetes.io/name: wit-stage
  name: wit-secret-stage-h98d6bhcm8
type: Opaque
---
...
```
