---
title: "Obtenir un certificat Let's Encrypt avec Certbot"
date: 2022-04-09T21:31:40+02:00
draft: false
tags: ['tls', "let's encrypt", 'certbot']
---

## Procédure pour mise en place sur une debian 11 (bullseye)

Nous commençons par installer le paquet `cerbot` :

```nohighlight
sudo apt update && sudo install -y certbot
```

Puis nous pouvons effectuer la demande de certificat :

```nohighlight
sudo certbot certonly \
             -d hola-34-77-205-178.nip.io \
             --non-interactive \
             --standalone \
             --agree-tos \
             --email me@example.com

Saving debug log to /var/log/letsencrypt/letsencrypt.log
Plugins selected: Authenticator standalone, Installer None
Obtaining a new certificate
Performing the following challenges:
http-01 challenge for hola-34-77-205-178.nip.io
Waiting for verification...
Cleaning up challenges

IMPORTANT NOTES:
 - Congratulations! Your certificate and chain have been saved at:
   /etc/letsencrypt/live/hola-34-77-205-178.nip.io/fullchain.pem
   Your key file has been saved at:
   /etc/letsencrypt/live/hola-34-77-205-178.nip.io/privkey.pem
   Your cert will expire on 2022-07-12.
   ...
```

Il est possible d'ajouter l'option `--test-cert` pour tester en premier sur le serveur de staging de letsencrypt qui n'est pas soumis aux mêmes quotas que le serveur de production.

A noter qu'en utilisant des services DNS dynamiques tels que [nip.io](https://nip.io/) ou [sslip.io](https://sslip.io/), le quota sur le nombre de certificats émis est __partagé entre tous les utilisateurs du service__, et cette limite est déjà souvent atteinte lorsque l'on tente soi-même de récupérer un certificat pour un sous-domaine de ces domaines.

## Outils utiles

### letsdebug

Ce service permet de simuler la création d'un certificat Letsencrypt et de détecter les raisons (mauvaise configuration DNS, serveur non accessible, [quotas déjà atteint](https://letsencrypt.org/fr/docs/rate-limits/)) qui peuvent empêcher cette création.

https://letsdebug.net


### crt

Ce service permet de voir l'historique des certificats émis pour un domaine particulier.

https://crt.sh


### unboundtest

Cet autre service permet de vérifier si le domaine pour lequel on souhaite obtenir un certificat est compatible avec letsencrypt ACME.

https://unboundtest.com

