---
title: "Configurer une datasource ClickHouse dans Grafana"
date: 2023-07-04T08:09:24+02:00
draft: false
tags: ['grafana', "clickhouse", 'kubernetes', 'helm']
---

## Introduction

Nous allons voir comment configurer une datasource de type [ClickHouse](https://clickhouse.com/) dans[Grafana](https://grafana.com/oss/grafana/).

Historiquement développé par la société russe Yandex et publié sous license libre depuis 2016, ClickHouse est une base de données conçue pour la haute performance qui permet l'analyse de données mises à jour en temps réel. ClickHouse est notamment utilisé par Cloudflare pour stocker et traiter les logs provenant de ses serveurs DNS.

## Gestion de l'accès à Clickhouse

Pour accéder à la base Clickhouse qui est un service managé externe au cluster Kubernetes nous avons besoin de créer les ressources suivantes dans le cluster.

Un Service sans Selector pour accéder au serveur Clickhouse depuis l'intérieur du cluster Kubernetes en utilisant le nom DNS `clickhouse` :

{{% highlight-bundle-file "clickhouse.service.yaml" "yaml" %}}

Un EndpointSlice pour indiquer l'adresse IP et le port d'écoute réel du serveur Clickhouse à l'extérieur du cluster Kubernetes :

{{% highlight-bundle-file "clickhouse.endpointslice.yaml" "yaml" %}}

Un Secret pour stocker les crédentiels nécessaires pour la connexion à la base de données CLickhouse :

{{% highlight-bundle-file "clickhouse.secret.yaml" "yaml" %}}

## Configuration de Grafana

Clickhouse ne fait pas partie des Datasources proposées par défaut dans Grafana, nous devons donc passer par l'installation du [plugin ClickHouse](https://grafana.com/grafana/plugins/grafana-clickhouse-datasource/).

Pour charger automatiquement ce plugin dans notre instance Grafana, nous pouvons configurer le fichier de valeurs de notre chart Helm Grafana comme suit :

{{% highlight-bundle-file "grafana.values.yaml" "yaml" %}}

Puis nous déployons Grafana via le Chart Helm officiel :

```none
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update
helm install mon-grafana grafana/grafana \
  --namespace mon-namespace
  --values grafana.values.yaml \
  --set rbac.create=false
```

Après connexion sur l'IHM de Grafana, la datasource devrait être directement fonctionnelle.
